//* Описать своими словами в несколько строчек, зачем в программировании нужны циклы.

// Циклы - это способ выполнять один и тот же кусок кода множество раз.
// Нужны, чтобы сократить количество кода, а также уменьшить время на выполнение задачи.

let userNumber = +prompt('Enter the number');

while (userNumber < 0 || userNumber % 1 !== 0) {
    userNumber = +prompt('Enter the number');
}
for (let defaultNumber = 0; defaultNumber <= userNumber; defaultNumber += 5) {
    if (userNumber < 5) {
        console.log('Sorry, no numbers');
    } else {
        console.log(defaultNumber);
    }
}

// Часть 2

// let n = 37;
//
// nextPrime:
//     for (let m = 11; m <= n; m++) { //
//
//         for (let j = 2; j < m; j++) {
//             if (m % j == 0) continue nextPrime;
//         }
//         console.log( m );
//     }
